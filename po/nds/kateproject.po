# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-23 00:49+0000\n"
"PO-Revision-Date: 2014-09-18 23:28+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr ""

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr ""

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr ""

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr ""

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr ""

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Branch"
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Last Commit"
msgstr ""

#: branchdeletedialog.cpp:135 kateprojecttreeviewcontextmenu.cpp:80
#, kde-format
msgid "Delete"
msgstr ""

#: branchdeletedialog.cpp:140
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] ""
msgstr[1] ""

#: branchesdialog.cpp:102
#, kde-format
msgid "Select Branch..."
msgstr ""

#: branchesdialog.cpp:128 gitwidget.cpp:533 kateprojectpluginview.cpp:70
#, fuzzy, kde-format
#| msgid "&Git"
msgid "Git"
msgstr "&Git"

#: comparebranchesview.cpp:145
#, kde-format
msgid "Back"
msgstr ""

#: currentgitbranchbutton.cpp:123
#, kde-format
msgctxt "Tooltip text, describing that '%1' commit is checked out"
msgid "HEAD at commit %1"
msgstr ""

#: currentgitbranchbutton.cpp:125
#, kde-format
msgctxt "Tooltip text, describing that '%1' tag is checked out"
msgid "HEAD is at this tag %1"
msgstr ""

#: currentgitbranchbutton.cpp:127
#, kde-format
msgctxt "Tooltip text, describing that '%1' branch is checked out"
msgid "Active branch: %1"
msgstr ""

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr ""

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:261
#, kde-format
msgid "Commit"
msgstr ""

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr ""

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr ""

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr ""

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr ""

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr ""

#: gitcommitdialog.cpp:112 gitwidget.cpp:1004
#, kde-format
msgid "Amend Last Commit"
msgstr ""

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr ""

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr ""

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr ""

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr ""

#: gitstatusmodel.cpp:87
#, fuzzy, kde-format
#| msgid "<untracked>"
msgid "Untracked"
msgstr "<nich verfolgt>"

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr ""

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr ""

#: gitwidget.cpp:269
#, kde-format
msgid "Git Push"
msgstr ""

#: gitwidget.cpp:282
#, kde-format
msgid "Git Pull"
msgstr ""

#: gitwidget.cpp:295
#, kde-format
msgid "Cancel Operation"
msgstr ""

#: gitwidget.cpp:303
#, kde-format
msgid " canceled."
msgstr ""

#: gitwidget.cpp:324 kateprojectview.cpp:59
#, kde-format
msgid "Filter..."
msgstr ""

#: gitwidget.cpp:412
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""

#: gitwidget.cpp:617
#, kde-format
msgid " error: %1"
msgstr ""

#: gitwidget.cpp:623
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr ""

#: gitwidget.cpp:643
#, kde-format
msgid "Failed to stage file. Error:"
msgstr ""

#: gitwidget.cpp:656
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr ""

#: gitwidget.cpp:667
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr ""

#: gitwidget.cpp:678
#, kde-format
msgid "Failed to remove. Error:"
msgstr ""

#: gitwidget.cpp:694
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr ""

#: gitwidget.cpp:726
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr ""

#: gitwidget.cpp:793
#, kde-format
msgid "Failed to commit: %1"
msgstr ""

#: gitwidget.cpp:797
#, kde-format
msgid "Changes committed successfully."
msgstr ""

#: gitwidget.cpp:807
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr ""

#: gitwidget.cpp:820
#, kde-format
msgid "Commit message cannot be empty."
msgstr ""

#: gitwidget.cpp:936
#, kde-format
msgid "No diff for %1...%2"
msgstr ""

#: gitwidget.cpp:942
#, kde-format
msgid "Failed to compare %1...%2"
msgstr ""

#: gitwidget.cpp:957
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr ""

#: gitwidget.cpp:996
#, kde-format
msgid "Refresh"
msgstr ""

#: gitwidget.cpp:1012
#, kde-format
msgid "Checkout Branch"
msgstr ""

#: gitwidget.cpp:1024
#, kde-format
msgid "Delete Branch"
msgstr ""

#: gitwidget.cpp:1036
#, kde-format
msgid "Compare Branch with..."
msgstr ""

#: gitwidget.cpp:1040
#, kde-format
msgid "Show Commit"
msgstr ""

#: gitwidget.cpp:1040
#, kde-format
msgid "Commit hash"
msgstr ""

#: gitwidget.cpp:1045
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Commit..."
msgstr "Opmaken mit"

#: gitwidget.cpp:1048 gitwidget.cpp:1087
#, kde-format
msgid "Stash"
msgstr ""

#: gitwidget.cpp:1058
#, kde-format
msgid "Diff - stash"
msgstr ""

#: gitwidget.cpp:1091
#, kde-format
msgid "Pop Last Stash"
msgstr ""

#: gitwidget.cpp:1095
#, kde-format
msgid "Pop Stash"
msgstr ""

#: gitwidget.cpp:1099
#, kde-format
msgid "Apply Last Stash"
msgstr ""

#: gitwidget.cpp:1101
#, kde-format
msgid "Stash (Keep Staged)"
msgstr ""

#: gitwidget.cpp:1105
#, kde-format
msgid "Stash (Include Untracked)"
msgstr ""

#: gitwidget.cpp:1109
#, kde-format
msgid "Apply Stash"
msgstr ""

#: gitwidget.cpp:1110
#, kde-format
msgid "Drop Stash"
msgstr ""

#: gitwidget.cpp:1111
#, kde-format
msgid "Show Stash Content"
msgstr ""

#: gitwidget.cpp:1151
#, kde-format
msgid "Stage All"
msgstr ""

#: gitwidget.cpp:1153
#, kde-format
msgid "Remove All"
msgstr ""

#: gitwidget.cpp:1153
#, kde-format
msgid "Discard All"
msgstr ""

#: gitwidget.cpp:1156
#, kde-format
msgid "Open .gitignore"
msgstr ""

#: gitwidget.cpp:1157 gitwidget.cpp:1212 gitwidget.cpp:1262
#: kateprojectconfigpage.cpp:89 kateprojectconfigpage.cpp:100
#, kde-format
msgid "Show Diff"
msgstr ""

#: gitwidget.cpp:1174
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr ""

#: gitwidget.cpp:1183
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr ""

#: gitwidget.cpp:1211
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open File"
msgstr "Opmaken mit"

#: gitwidget.cpp:1213
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr ""

#: gitwidget.cpp:1214
#, kde-format
msgid "Open at HEAD"
msgstr ""

#: gitwidget.cpp:1215
#, kde-format
msgid "Unstage File"
msgstr ""

#: gitwidget.cpp:1215
#, kde-format
msgid "Stage File"
msgstr ""

#: gitwidget.cpp:1216
#, kde-format
msgid "Remove"
msgstr ""

#: gitwidget.cpp:1216
#, kde-format
msgid "Discard"
msgstr ""

#: gitwidget.cpp:1233
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr ""

#: gitwidget.cpp:1246
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr ""

#: gitwidget.cpp:1261
#, kde-format
msgid "Unstage All"
msgstr ""

#: gitwidget.cpp:1328
#, kde-format
msgid "Unstage Selected Files"
msgstr ""

#: gitwidget.cpp:1328
#, kde-format
msgid "Stage Selected Files"
msgstr ""

#: gitwidget.cpp:1329
#, kde-format
msgid "Discard Selected Files"
msgstr ""

#: gitwidget.cpp:1333
#, kde-format
msgid "Remove Selected Files"
msgstr ""

#: gitwidget.cpp:1346
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr ""

#: gitwidget.cpp:1355
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr ""

#: kateproject.cpp:217
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr ""

#: kateproject.cpp:541
#, kde-format
msgid "<untracked>"
msgstr "<nich verfolgt>"

#: kateprojectcompletion.cpp:44
#, kde-format
msgid "Project Completion"
msgstr "Projektkompletteren"

#: kateprojectconfigpage.cpp:25
#, fuzzy, kde-format
#| msgid "Autoload repositories"
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr "Archiven automaatsch laden"

#: kateprojectconfigpage.cpp:27
#, fuzzy, kde-format
#| msgid ""
#| "Project plugin is able to autoload repository working copies in case "
#| "there is .kateproject file already defined"
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""
"Dat Projektmoduul kann automaatsch Arbeitkopien ut Archiven laden, wenn dat "
"al en .kateproject-Datei gifft"

#: kateprojectconfigpage.cpp:30
#, kde-format
msgid "&Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:33
#, kde-format
msgid "&Subversion"
msgstr "&Subversion"

#: kateprojectconfigpage.cpp:35
#, kde-format
msgid "&Mercurial"
msgstr "&Mercurial"

#: kateprojectconfigpage.cpp:37
#, kde-format
msgid "&Fossil"
msgstr ""

#: kateprojectconfigpage.cpp:46
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr ""

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Session settings for projects"
msgstr ""

#: kateprojectconfigpage.cpp:48
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Restore Open Projects"
msgstr "Dat aktuelle Projekt"

#: kateprojectconfigpage.cpp:55
#, fuzzy, kde-format
#| msgid "Projects"
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "Projekten"

#: kateprojectconfigpage.cpp:56
#, kde-format
msgid "Project ctags index settings"
msgstr ""

#: kateprojectconfigpage.cpp:57 kateprojectinfoviewindex.cpp:208
#, kde-format
msgid "Enable indexing"
msgstr ""

#: kateprojectconfigpage.cpp:60
#, kde-format
msgid "Directory for index files"
msgstr ""

#: kateprojectconfigpage.cpp:64
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""

#: kateprojectconfigpage.cpp:71
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr ""

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""

#: kateprojectconfigpage.cpp:73
#, fuzzy, kde-format
#| msgid "Project Completion"
msgid "Cross-Project Completion"
msgstr "Projektkompletteren"

#: kateprojectconfigpage.cpp:75
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr ""

#: kateprojectconfigpage.cpp:83
#, fuzzy, kde-format
#| msgid "&Git"
msgctxt "Groupbox title"
msgid "Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:86
#, kde-format
msgid "Single click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:88 kateprojectconfigpage.cpp:99
#, kde-format
msgid "No Action"
msgstr ""

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:101
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open file"
msgstr "Opmaken mit"

#: kateprojectconfigpage.cpp:91 kateprojectconfigpage.cpp:102
#, kde-format
msgid "Stage / Unstage"
msgstr ""

#: kateprojectconfigpage.cpp:97
#, kde-format
msgid "Double click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:132 kateprojectpluginview.cpp:69
#, kde-format
msgid "Projects"
msgstr "Projekten"

#: kateprojectconfigpage.cpp:137
#, fuzzy, kde-format
#| msgid "Projects properties"
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "Projektegenschappen"

#: kateprojectinfoview.cpp:35
#, kde-format
msgid "Terminal (.kateproject)"
msgstr ""

#: kateprojectinfoview.cpp:43
#, fuzzy, kde-format
#| msgid "Terminal"
msgid "Terminal (Base)"
msgstr "Konsool"

#: kateprojectinfoview.cpp:50
#, kde-format
msgid "Code Index"
msgstr "Kodeindex"

#: kateprojectinfoview.cpp:55
#, kde-format
msgid "Code Analysis"
msgstr "Kodeanalyseren"

#: kateprojectinfoview.cpp:60
#, kde-format
msgid "Notes"
msgstr "Notizen"

#: kateprojectinfoviewcodeanalysis.cpp:34
#, kde-format
msgid "Start Analysis..."
msgstr "Analyseren anfangen"

#: kateprojectinfoviewcodeanalysis.cpp:41
#, fuzzy, kde-format
#| msgid "Code Analysis"
msgctxt "'%1' refers to project name, e.g,. Code Analysis - MyProject"
msgid "Code Analysis - %1"
msgstr "Kodeanalyseren"

#: kateprojectinfoviewcodeanalysis.cpp:100
#, kde-format
msgid ""
"%1<br/><br/>The tool will be run on all project files which match this list "
"of file extensions:<br/><br/><b>%2</b>"
msgstr ""

#: kateprojectinfoviewcodeanalysis.cpp:138
#: kateprojectinfoviewcodeanalysis.cpp:198
#: kateprojectinfoviewcodeanalysis.cpp:202
#, fuzzy, kde-format
#| msgid "Code Analysis"
msgid "CodeAnalysis"
msgstr "Kodeanalyseren"

#: kateprojectinfoviewcodeanalysis.cpp:193
#, kde-format
msgctxt ""
"Message to the user that analysis finished. %1 is the name of the program "
"that did the analysis, %2 is a number. e.g., [clang-tidy]Analysis on 5 files "
"finished"
msgid "[%1]Analysis on %2 file finished."
msgid_plural "[%1]Analysis on %2 files finished."
msgstr[0] ""
msgstr[1] ""

#: kateprojectinfoviewcodeanalysis.cpp:201
#, kde-format
msgid "Analysis failed with exit code %1, Error: %2"
msgstr ""

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Name"
msgstr "Naam"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Kind"
msgstr "Oort"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "File"
msgstr "Datei"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Line"
msgstr "Reeg"

#: kateprojectinfoviewindex.cpp:36
#, kde-format
msgid "Search"
msgstr "Söken"

#: kateprojectinfoviewindex.cpp:197
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "De Index lett sik nich opstellen. Installeer bitte \"ctags\"."

#: kateprojectinfoviewindex.cpp:207
#, kde-format
msgid "Indexing is not enabled"
msgstr ""

#: kateprojectitem.cpp:176
#, kde-format
msgid "Error"
msgstr ""

#: kateprojectitem.cpp:176
#, kde-format
msgid "File name already exists"
msgstr ""

#: kateprojectplugin.cpp:217
#, kde-format
msgid "Confirm project closing: %1"
msgstr ""

#: kateprojectplugin.cpp:218
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr ""

#: kateprojectplugin.cpp:557
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr ""

#: kateprojectplugin.cpp:574
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""

#: kateprojectplugin.cpp:691 kateprojectpluginview.cpp:75
#: kateprojectpluginview.cpp:227 kateprojectviewtree.cpp:136
#: kateprojectviewtree.cpp:157
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project"
msgstr "Projekten"

#: kateprojectpluginview.cpp:59
#, fuzzy, kde-format
#| msgid "Kate Project Manager"
msgid "Project Manager"
msgstr "Kate-Projektpleger"

#: kateprojectpluginview.cpp:81
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Open projects list"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:86
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Reload project"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:89
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close project"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:104
#, kde-format
msgid "Refresh git status"
msgstr ""

#: kateprojectpluginview.cpp:182
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Folder..."
msgstr "Opmaken mit"

#: kateprojectpluginview.cpp:187
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project TODOs"
msgstr "Projekten"

#: kateprojectpluginview.cpp:191
#, kde-format
msgid "Activate Previous Project"
msgstr ""

#: kateprojectpluginview.cpp:196
#, kde-format
msgid "Activate Next Project"
msgstr ""

#: kateprojectpluginview.cpp:201
#, kde-format
msgid "Lookup"
msgstr ""

#: kateprojectpluginview.cpp:205
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Project"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:209
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close All Projects"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:214
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Orphaned Projects"
msgstr "Dat aktuelle Projekt"

#: kateprojectpluginview.cpp:224
#, kde-format
msgid "Checkout Git Branch"
msgstr ""

#: kateprojectpluginview.cpp:230 kateprojectpluginview.cpp:811
#, kde-format
msgid "Lookup: %1"
msgstr ""

#: kateprojectpluginview.cpp:231 kateprojectpluginview.cpp:812
#, kde-format
msgid "Goto: %1"
msgstr ""

#: kateprojectpluginview.cpp:312
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Projects Index"
msgstr "Projekten"

#: kateprojectpluginview.cpp:856
#, kde-format
msgid "Choose a directory"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:44
#, kde-format
msgid "Enter name:"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:45
#, kde-format
msgid "Add"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:66
#, kde-format
msgid "Copy Location"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:71
#, fuzzy, kde-format
#| msgid "File"
msgid "Add File"
msgstr "Datei"

#: kateprojecttreeviewcontextmenu.cpp:72
#, kde-format
msgid "Add Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:79
#, kde-format
msgid "&Rename"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:86
#, fuzzy, kde-format
#| msgid "Projects properties"
msgid "Properties"
msgstr "Projektegenschappen"

#: kateprojecttreeviewcontextmenu.cpp:90
#, kde-format
msgid "Open With"
msgstr "Opmaken mit"

#: kateprojecttreeviewcontextmenu.cpp:98
#, kde-format
msgid "Open Internal Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:107
#, kde-format
msgid "Open External Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:112
#, kde-format
msgid "&Open Containing Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:122
#, kde-format
msgid "Show Git History"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:132
#, kde-format
msgid "Delete File"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:133
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr ""

#: kateprojectviewtree.cpp:136
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr ""

#: kateprojectviewtree.cpp:157
#, kde-format
msgid "Failed to create dir: %1"
msgstr ""

#: stashdialog.cpp:37
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""

#: stashdialog.cpp:44
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""

#: stashdialog.cpp:143
#, kde-format
msgid "Failed to stash changes %1"
msgstr ""

#: stashdialog.cpp:145
#, kde-format
msgid "Changes stashed successfully."
msgstr ""

#: stashdialog.cpp:164
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr ""

#: stashdialog.cpp:180
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr ""

#: stashdialog.cpp:182
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr ""

#: stashdialog.cpp:184
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr ""

#: stashdialog.cpp:188
#, kde-format
msgid "Stash applied successfully."
msgstr ""

#: stashdialog.cpp:190
#, kde-format
msgid "Stash dropped successfully."
msgstr ""

#: stashdialog.cpp:192
#, kde-format
msgid "Stash popped successfully."
msgstr ""

#: stashdialog.cpp:221
#, kde-format
msgid "Show stash failed. Error: "
msgstr ""

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "&Projekten"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sönke Dibbern"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "s_dibbern@web.de"

#, fuzzy
#~| msgid "Copy Filename"
#~ msgid "Copy File Path"
#~ msgstr "Dateinaam koperen"

#, fuzzy
#~| msgid "Open With"
#~ msgid "Type to filter..."
#~ msgstr "Opmaken mit"

#, fuzzy
#~| msgid "<untracked>"
#~ msgid "Untracked (%1)"
#~ msgstr "<nich verfolgt>"

#~ msgid "Please install 'cppcheck'."
#~ msgstr "Installeer bitte \"cppcheck\"."

#~ msgid "Git Tools"
#~ msgstr "Git-Warktüüch"

#~ msgid "Launch gitk"
#~ msgstr "Gitk opropen"

#~ msgid "Launch qgit"
#~ msgstr "Qgit opropen"

#~ msgid "Launch git-cola"
#~ msgstr "Git-cola opropen"

#~ msgid "Hello World"
#~ msgstr "Moin Welt"

#~ msgid "Example kate plugin"
#~ msgstr "Bispillmoduul för Kate"
